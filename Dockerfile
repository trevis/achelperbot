FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /source

# copy csproj and restore as distinct layers
COPY ./*.csproj .
RUN dotnet restore

# copy and publish app and libraries
COPY ./ .
RUN dotnet publish -c Release -o /app --no-restore

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS base
WORKDIR /app
COPY --from=build /app .

# set git metadata
ARG SET_VERSION=""
ENV GIT_VERSION=${SET_VERSION}

ENTRYPOINT ["./ACHelperBot"]
