﻿using System;

namespace ACHelperBot.Models {
    public class EmbedLineItem : IComparable {
        public string Text { get; set; }
        public int Priority { get; set; }

        public EmbedLineItem(string text, int priority = 0) {
            Text = text;
            Priority = priority;
        }

        public int CompareTo(object obj) {
            return Priority.CompareTo((obj as EmbedLineItem).Priority);
        }
    }
}