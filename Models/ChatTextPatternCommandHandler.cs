﻿using ACHelperBot.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Models {
    public class ChatTextPatternCommandHandler {
        public Type Type { get; }
        public Regex Pattern { get; }
        public MethodInfo Handler { get; }

        public ChatTextPatternCommandHandler(Type type, Regex pattern, MethodInfo handler) {
            Type = type;
            Pattern = pattern;
            Handler = handler;
        }
    }
}
