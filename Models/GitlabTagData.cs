﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Models {
    public class GitLabTagData {
        public string name = "";
        public string tag_name = "";
        public DateTime created_at = DateTime.MinValue;
        public DateTime released_at = DateTime.MinValue;
        public string description = "";

    }
}
