﻿
using ACHelperBot.Lib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ACHelperBot.Models {
    /*
        Microsoft XML Libraries
        [msxml3.dll]	: Installed (8.110.18362.239)
        [msxml4.dll]	: Not Installed
        [msxml5.dll]	: Not Installed
        [msxml6.dll]	: Installed (6.30.18362.418)
        [v1.0.3705] (1.0 Final)	: Not Installed
        [v1.1.4322] (1.1 Final)	: Not Installed
        [v2.0.50215] (2.0 Beta 2)	: Not Installed
        [v2.0.50727] (2.0 Final)	: Installed (Service Pack 2)
        [v3.0] (3.0)		: Installed (Service Pack 2)
        [v3.5] (3.5)		: Installed (Service Pack 1)
        [v4] (4.0)			: Installed (Version: 4.8.03752)
     */
    class XMLLibraries {
        public struct XMLLibrary {
            public string Name { get; private set; }
            public bool Installed { get; private set; }
            public Version InstalledVersion { get; private set; }

            public XMLLibrary(string name, bool installed, Version version) {
                Name = name;
                Installed = installed;
                InstalledVersion = version;
            }
        }


        Regex VersionRegex = new Regex(@"[0-9\.]+");

        /// <summary>
        /// List of installed C++ libraries
        /// </summary>
        public List<XMLLibrary> Libraries { get; private set; } = new List<XMLLibrary>();

        public int Parse(string[] lines, int currentLine) {
            while (currentLine < lines.Length) {
                currentLine++;
                var line = lines[currentLine];

                if (!line.StartsWith("[") || !line.Contains(":"))
                    break;

                (string name, string value) = ParseHelpers.ParseKV(line);

                if (name == null)
                    break;

                bool installed = value.StartsWith("Installed");
                var v = value.Contains("(") ? value.Split("(")[1].Replace(")", "").Trim() : "0.0.0.0";
                Version version = installed && VersionRegex.IsMatch(v) ? new Version(v) : new Version("0.0.0.0");

                Libraries.Add(new XMLLibrary(name, installed, version));
            }

            return currentLine - 1;
        }

        public void Print() {
            Console.WriteLine("XML Libraries");
            foreach (var lib in Libraries) {
                Console.WriteLine($"\t{lib.Name} : {(lib.Installed ? $"Installed ({lib.InstalledVersion})" : "Not Installed")}");
            }
        }

        /// <summary>
        /// Check if the specified XML library is installed. ie: HasLibrary("d3dx9_30.dll")
        /// </summary>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public bool IsInstalled(string libraryName) {
            return Libraries.Find(x => x.Name.ToLower() == libraryName).Installed;
        }

        /// <summary>
        /// Returns the version of the specified XML library
        /// </summary>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public Version GetVersion(string libraryName) {
            return Libraries.Find(x => x.Name.ToLower() == libraryName).InstalledVersion;
        }
    }
}
