﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Models {
    public class EmbedField {
        /// <summary>
        /// Field Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// List of line items to be included under this field
        /// </summary>
        public List<EmbedLineItem> LineItems { get; } = new List<EmbedLineItem>();

        /// <summary>
        /// Set to true to display Key/Value on the same line
        /// </summary>
        public bool Inline { get; set; }

        /// <summary>
        /// Priority in this Embed Field 
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Line item seperator characters
        /// </summary>
        public string LineItemSeperator { get; set; } = "\n";


        public EmbedField(string key, bool inline = false, int priority = 0) {
            Title = key;
            Inline = inline;
            Priority = priority;

            switch (key) {
                case "Errors":
                    Priority = 100000;
                    break;
                case "Warnings":
                    Priority = 10000;
                    break;
                case "Decal Update":
                    Priority = 1000;
                    break;
                case "Out of Date Plugins":
                    Priority = -1000;
                    break;
                case "Tips":
                    Priority = -100000;
                    break;
            }
        }

        public void AddLineItem(EmbedLineItem item) {
            LineItems.Add(item);
        }
    }
}
