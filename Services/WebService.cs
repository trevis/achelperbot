﻿using EmbedIO.Actions;
using EmbedIO;
using RazorLight;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmbedIO.WebApi;
using EmbedIO.Routing;
using ACE.DatLoader.Entity;
using Discord.WebSocket;
using Discord;
using System.Net;
using System.Net.Http;
using System.IO;
using ACClientLib.Lib.Networking;
using ACClientLib.Lib.Networking.Packets;
using ACClientLib.Lib;
using System.Threading;
using Medo.IO.Pcap;

namespace ACHelperBot.Services {
    public class WebService : WebModuleBase {
        public RazorLightEngine Engine { get; }

        public WebServer Server { get; }

        public override bool IsFinalHandler => true;

        public DiscordSocketClient Client { get; }

        private DecalExportService _exportService;
        private HttpClient _webClient;

        public WebService(DiscordSocketClient client, DecalExportService exportService) : base("/") {
            Client = client;
            _exportService = exportService;

            Engine = new RazorLightEngineBuilder()
                .UseEmbeddedResourcesProject(typeof(WebService).Assembly, "ACHelperBot.WebViews")
                .Build();

            Server = new WebServer(o => o
                .WithUrlPrefix("http://+:5001")

                .WithMode(HttpListenerMode.EmbedIO))
                .WithModule(this);

            client.MessageReceived += Client_MessageReceived;
            _webClient = new HttpClient();
            Server.RunAsync();
        }

        private Task Client_MessageReceived(SocketMessage message) {
            if (message.Author == null || message.Channel == null || message.Author.IsBot || !(message.Channel is SocketGuildChannel guildChannel))
                return Task.CompletedTask;

            _ = Task.Run(async () => {
                foreach (var attachment in message.Attachments) {
                    // only looking at txt files and files with no extension
                    if (attachment.Filename.EndsWith(".pcap")) {

                        await message.Channel.SendMessageAsync($"View this pcap online [here](https://supportbot.utilitybelt.me/pcap?channel={message.Channel.Id}&msg={message.Id}).", false, null, null, null, new MessageReference(message.Id));
                    }
                }
            });

            return Task.CompletedTask;
        }

        protected override Task OnRequestAsync(IHttpContext context) {
            if (context.RequestedPath == "/decalexport") {
                return HandleDecalExport(context);
            }
            else if (context.RequestedPath == "/pcap") {
                return HandlePcap(context);
            }

            return context.SendStandardHtmlAsync(404);
        }

        private async Task HandlePcap(IHttpContext context) {
            var channelIdStr = context.GetRequestQueryData().Get("channel");
            var msgIdStr = context.GetRequestQueryData().Get("msg");

            if (ulong.TryParse(channelIdStr, out var channelId) && ulong.TryParse(msgIdStr, out var msgId)) {
                var channel = (ITextChannel)await Client.GetChannelAsync(channelId);
                var message = await channel?.GetMessageAsync(msgId);
                if (message != null) {
                    string? pcapUrl = null;
                    foreach (var attachment in message.Attachments) {
                        // only looking at txt files and files with no extension
                        if (attachment.Filename.EndsWith(".pcap")) {
                            pcapUrl = attachment.Url;
                            break;
                        }
                    }
                    if (pcapUrl != null) {
                        var pcapData = await _webClient.GetAsync(pcapUrl);
                        if (pcapData != null) {
                            using (var stream = pcapData.Content.ReadAsStream())
                            using (var reader = new Medo.IO.Pcap.PcapReader(stream)) {
                                Console.WriteLine($"Pcap Format: {reader.Format}");
                                var netManager = new NetworkManager(null);
                                var messageHandler = new UtilityBelt.Common.Messages.MessageHandler();
                                var model = new PcapViewModel();
                                netManager.OnIncomingMessage += (s, e) => {
                                    messageHandler.HandleIncomingMessageData(e.Data);
                                };
                                netManager.OnOutgoingMessage += (s, e) => {
                                    messageHandler.HandleOutgoingMessageData(e.Data);
                                };
                                var nextId = 0;
                                messageHandler.Incoming.Message += (s, e) => {
                                    model.Messages.Add(new PcapMessage() {
                                        Id = nextId++,
                                        Type = e.Type.ToString(),
                                        Data = e.Data,
                                        Time = e.Time,
                                        Direction = "Recv"
                                    });
                                };
                                messageHandler.Outgoing.Message += (s, e) => {
                                    model.Messages.Add(new PcapMessage() {
                                        Id = nextId++,
                                        Type = e.Type.ToString(),
                                        Data = e.Data,
                                        Time = e.Time,
                                        Direction = "Send"
                                    });
                                };
                                var block = reader.NextBlock(true);
                                var si = new ServerInfo("", 9000);
                                while (block != null) {
                                    if (block is not PcapClassicPacket packet) {
                                        block = reader.NextBlock();
                                        continue;
                                    }
                                    netManager.ProcessPacket(packet.GetData().Skip(42).ToArray(), si);
                                    block = reader.NextBlock();
                                }
                                string html = await Engine.CompileRenderAsync("Pcap", model);
                                await context.SendStringAsync(html, "text/html", Encoding.UTF8);
                                return;
                                /*
                                var netManager = new NetworkManager(null);
                                netManager.OnIncomingMessage += (s, e) => {
                                    model.Messages.Add(new PcapMessage() {
                                        Data = e.Data,
                                        Direction = "Incoming"
                                    });
                                };
                                netManager.OnOutgoingMessage += (s, e) => {
                                    model.Messages.Add(new PcapMessage() {
                                        Data = e.Data,
                                        Direction = "Outgoing"
                                    });
                                };

                                var si = new ServerInfo("", 9000);
                                foreach (var packet in packets) {
                                    //netManager.ProcessPacket(packet.Data, si);
                                    ReadPacket(packet);
                                }
                                var packets = pcap.Sections.SelectMany(section => section.Blocks)
                                    .Where(block => block.Body.GetType() == typeof(SimplePacketBlockBody))
                                    .Select(b => b.Body)
                                    .Cast<SimplePacketBlockBody>();
                                foreach (var packet in packets) {
                                    //netManager.ProcessPacket(packet.Data, si);
                                    ReadPacket(packet);
                                }
                                */
                            }
                        }
                    }
                }
            }

            await context.SendStandardHtmlAsync(404);
            return;
        }

        private async Task HandleDecalExport(IHttpContext context) {
            var channelIdStr = context.GetRequestQueryData().Get("channel");
            var msgIdStr = context.GetRequestQueryData().Get("msg");

            if (ulong.TryParse(channelIdStr, out var channelId) && ulong.TryParse(msgIdStr, out var msgId)) {
                var channel = (ITextChannel)await Client.GetChannelAsync(channelId);
                var message = await channel?.GetMessageAsync(msgId);
                if (message != null) {
                    var export = (await _exportService.GetDecalExportResponses(message)).FirstOrDefault();
                    if (export != null) {
                        string html = await Engine.CompileRenderAsync("DecalExport", export);
                        await context.SendStringAsync(html, "text/html", Encoding.UTF8);
                    }
                }
                return;
            }

            await context.SendStandardHtmlAsync(404);
            return;
        }
    }

    public class PcapViewModel {
        public List<PcapMessage> Messages { get; } = new List<PcapMessage>();
    }

    public class PcapMessage {
        public string Type { get; set; }
        public object Data { get; set; }
        public DateTime Time { get; set; }
        public string Direction { get; set; }
        public object Id { get; internal set; }
    }
}
