﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Services {
    public interface ILogService {
        public Task LogAsync(LogMessage log);
    }
}
