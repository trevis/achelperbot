﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Modules;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Services {
    public class ModuleService {
        private readonly ILogService _log;
        private readonly InteractionService _interactions;
        private readonly IServiceProvider _services;
        private readonly DiscordSocketClient _client;
        private readonly ImageTextService _imageService;

        private readonly List<ChatTextPatternCommandHandler> _globalChatTextPatternHandlers = new List<ChatTextPatternCommandHandler>();
        private readonly Dictionary<ulong, List<ChatTextPatternCommandHandler>> _guildChatTextPatternHandlers = new Dictionary<ulong, List<ChatTextPatternCommandHandler>>();

        internal readonly List<DecalExportRuleHandler> GlobalDecalExportRuleHandlers = new List<DecalExportRuleHandler>();
        internal readonly Dictionary<ulong, List<DecalExportRuleHandler>> GuildDecalExportRuleHandlers = new Dictionary<ulong, List<DecalExportRuleHandler>>();


        public ModuleService(ILogService logger, InteractionService interactions, IServiceProvider services, DiscordSocketClient client, ImageTextService imageService) {
            _log = logger;
            _interactions = interactions;
            _services = services;
            _client = client;
            _imageService = imageService;

            _client.Ready += Client_Ready;
            _client.MessageReceived += Client_MessageReceived;

            _log.LogAsync(new LogMessage(LogSeverity.Verbose, "ModuleService", $"Initialized"));
        }

        private async Task Client_Ready() {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ModuleService", $"Loading Modules"));
            await LoadModules();
        }

        private Task Client_MessageReceived(SocketMessage message) {
            if (message.Author == null || message.Channel == null || message.Author.IsBot || !(message.Channel is SocketGuildChannel guildChannel))
                return Task.CompletedTask;

            _ = Task.Run(async () => {
                try {
                    var imageText = await _imageService.GetAnyImageText(message);
                    var globalResponse = await RunGlobalChatTextPatternHandlers(message, imageText);
                    var guildResponse = await RunGuildChatTextPatternHandlers(message, guildChannel.Guild.Id, imageText);

                    if (globalResponse != null || guildResponse != null) {
                        SupportMessage response = new SupportMessage("");
                        response.Merge(globalResponse);
                        response.Merge(guildResponse);
                        await message.Channel.SendMessageAsync(response.Text, false, response.Embed, null, null, response.MessageReference);
                    }
                }
                catch (Exception ex) {
                    await _log.LogAsync(new LogMessage(LogSeverity.Error, "ModuleService", $"Error running ChatTextPatternHandlers", ex));
                }
            });

            return Task.CompletedTask;
        }

        #region Chat Text Pattern Handlers
        private async Task<SupportMessage> RunGuildChatTextPatternHandlers(SocketMessage message, ulong guildId, string imageText) {
            if (!_guildChatTextPatternHandlers.ContainsKey(guildId))
                return null;

            return await RunChatTextPatternHandlers(message, _guildChatTextPatternHandlers[guildId], imageText);
        }

        private async Task<SupportMessage> RunGlobalChatTextPatternHandlers(SocketMessage message, string imageText) {
            return await RunChatTextPatternHandlers(message, _globalChatTextPatternHandlers, imageText);
        }

        private async Task<SupportMessage> RunChatTextPatternHandlers(SocketMessage message, List<ChatTextPatternCommandHandler> handlers, string imageText) {
            SupportMessage response = new SupportMessage("");
            bool hasResponse = false;

            foreach (var handler in handlers) {
                if (handler.Pattern.IsMatch(message.Content) || (!string.IsNullOrEmpty(imageText) && handler.Pattern.IsMatch(imageText))) {
                    try {
                        if (await RunChatTextPatternHandler(handler, response, message))
                            hasResponse = true;
                    }
                    catch (Exception ex) {
                        await _log.LogAsync(new LogMessage(LogSeverity.Error, "ModuleService", $"Failed to run ChatTextPatternHandler {handler.Type} ({handler.Pattern})", ex));
                    }
                }
            }

            return hasResponse ? response : null;
        }

        private async Task<bool> RunChatTextPatternHandler(ChatTextPatternCommandHandler handler, SupportMessage response, SocketMessage message) {
            var moduleInstance = _services.GetService(handler.Type);
            if (moduleInstance == null)
                return false;

            Task<SupportMessage> result = (Task<SupportMessage>)handler.Handler.Invoke(moduleInstance, new object[] { (IMessage)message });
            await result;

            if (result.Result != null) {
                response.Merge(result.Result);
                return true;
            }

            return false;
        }
        #endregion Chat Text Pattern Handlers

        /// <summary>
        /// Gets a list of all defined IModules in this assembly
        /// </summary>
        /// <returns></returns>
        internal static List<Type> GetModuleTypes() {
            var types = new List<Type>();

            var assembly = Assembly.GetEntryAssembly();
            foreach (var type in assembly.ExportedTypes) {
                if ((typeof(IGlobalModule).IsAssignableFrom(type) && type != typeof(IGlobalModule)) ||
                    (typeof(IGuildModule).IsAssignableFrom(type) && type != typeof(IGuildModule)) ||
                    (typeof(IDecalExportModule).IsAssignableFrom(type) && type != typeof(IDecalExportModule))) {
                    types.Add(type);
                }
            }

            return types;
        }

        private async Task LoadModules() {
            try {
                var types = GetModuleTypes();
                List<ModuleInfo> globalModules = new List<ModuleInfo>();
                Dictionary<ulong, List<ModuleInfo>> guildModules = new Dictionary<ulong, List<ModuleInfo>>();

                foreach (var type in types) {
                    //await _log.LogAsync(new LogMessage(LogSeverity.Verbose, "ModuleService", $"Loading module: {type.GetTypeInfo()}"));

                    var moduleInfo = _interactions.Modules.Where(m => m.Name == type.ToString().Split('.').Last()).FirstOrDefault();
                    if (moduleInfo == null)
                        continue;

                    if (typeof(IGlobalModule).IsAssignableFrom(type)) {
                        IGlobalModule globalModule = (IGlobalModule)_services.GetService(type);
                        if (globalModule != null && await LoadGlobalModule(type)) {
                            globalModules.Add(moduleInfo);
                        }
                    }
                    else if (typeof(IGuildModule).IsAssignableFrom(type)) {
                        IGuildModule guildModule = (IGuildModule)_services.GetService(type);
                        if (guildModule == null || !await LoadGuildModule(type, guildModule.GetGuildID()))
                            continue;

                        if (!guildModules.ContainsKey(guildModule.GetGuildID()))
                            guildModules.Add(guildModule.GetGuildID(), new List<ModuleInfo>());
                        guildModules[guildModule.GetGuildID()].Add(moduleInfo);
                    }
                    else if (typeof(IDecalExportModule).IsAssignableFrom(type)) {
                        IDecalExportModule decalExportModule = (IDecalExportModule)_services.GetService(type);
                        if (decalExportModule == null || !await LoadGlobalModule(type))
                            continue;
                    }
                    else {
                        await _log.LogAsync(new LogMessage(LogSeverity.Warning, "ModuleService", $"Unknown IModule: {type}"));
                    }
                }

                // register global modules
                await _log.LogAsync(new LogMessage(LogSeverity.Warning, "ModuleService", $"Global Modules: {globalModules.Count}"));
                await _interactions.AddModulesGloballyAsync(true, globalModules.ToArray());

                // register guild modules
                foreach (var kv in guildModules) {
                    var guild = _client.GetGuild(kv.Key);
                    await _interactions.AddModulesToGuildAsync(guild, true, kv.Value.ToArray());
                }
            }
            catch(Exception ex) {
                await _log.LogAsync(new Discord.LogMessage(Discord.LogSeverity.Critical, "ModuleService", "Error loading modules", ex));
            }
        }

        private async Task<bool> LoadGuildModule(Type type, ulong guildId) {
            var guild = _client.GetGuild(guildId);
            if (guild == null) {
                await _log.LogAsync(new LogMessage(LogSeverity.Warning, "ModuleService", $"Unable to load GuildModule for: {type} ({guildId}). No Guild Found."));
                return false;
            }

            var chatTextPatternHandlers = GetChatTextPatternCommandHandlers(type);
            if (chatTextPatternHandlers.Count > 0) {
                if (!_guildChatTextPatternHandlers.ContainsKey(guildId))
                    _guildChatTextPatternHandlers.Add(guildId, new List<ChatTextPatternCommandHandler>());

                _guildChatTextPatternHandlers[guildId].AddRange(chatTextPatternHandlers);
            }

            var decalExportRuleHandlers = GetDecalExportRuleHandlers(type);
            if (decalExportRuleHandlers.Count > 0) {
                if (!GuildDecalExportRuleHandlers.ContainsKey(guildId))
                    GuildDecalExportRuleHandlers.Add(guildId, new List<DecalExportRuleHandler>());

                GuildDecalExportRuleHandlers[guildId].AddRange(decalExportRuleHandlers);
            }

            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ModuleService", $"Loaded guild module for {guild.Name}: {type}"));

            return true;
        }

        private async Task<bool> LoadGlobalModule(Type type) {
            var chatTextPatternHandlers = GetChatTextPatternCommandHandlers(type);
            if (chatTextPatternHandlers.Count > 0)
                _globalChatTextPatternHandlers.AddRange(chatTextPatternHandlers);

            var decalExportRuleHandlers = GetDecalExportRuleHandlers(type);
            if (decalExportRuleHandlers.Count > 0)
                GlobalDecalExportRuleHandlers.AddRange(decalExportRuleHandlers);

            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ModuleService", $"Loaded global module: {type}"));

            return true;
        }

        private List<ChatTextPatternCommandHandler> GetChatTextPatternCommandHandlers(Type type) {
            var handlers = new List<ChatTextPatternCommandHandler>();
            var methods = type.GetMethods(BindingFlags.Default | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            
            foreach (var method in methods) {
                var chatTextPatternAttributes = method.GetCustomAttributes().Where(a => a is ChatTextPatternCommandAttribute).Select(a => a as ChatTextPatternCommandAttribute);

                foreach (var chatTextPatternMethod in chatTextPatternAttributes) {
                    handlers.Add(new ChatTextPatternCommandHandler(type, chatTextPatternMethod.MatchPattern, method));
                }
            }

            return handlers;
        }

        private List<DecalExportRuleHandler> GetDecalExportRuleHandlers(Type type) {
            var handlers = new List<DecalExportRuleHandler>();
            var methods = type.GetMethods(BindingFlags.Default | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var method in methods) {
                var decalExportRuleAttributes = method.GetCustomAttributes().Where(a => a is DecalExportRuleAttribute).Select(a => a as DecalExportRuleAttribute);

                foreach (var decalExportRuleAttribute in decalExportRuleAttributes) {
                    handlers.Add(new DecalExportRuleHandler(type, method));
                }
            }

            return handlers;
        }
    }
}
