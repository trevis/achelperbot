﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ACHelperBot.Services {
    /// <summary>
    /// Updates the bot's activity with a custom status of user count across all discord guilds
    /// 
    /// *Listening to* {userCount:N0} AC Players
    /// 
    /// Note: this number is a sum of the MemberCount of all joined discord guilds, and does not account
    /// for duplicate members
    /// </summary>
    class BotActivityService {
        private DiscordSocketClient _client;
        private ILogService _log;

        private string lastStatus = null;
        private bool isUpdatingUsers = false;

        public BotActivityService(DiscordSocketClient client, ILogService logger) {
            _client = client;
            _log = logger;
            _client.Ready += Client_Ready;

            _log.LogAsync(new LogMessage(LogSeverity.Verbose, "BotActivityService", $"Initialized"));
        }

        private Task Client_Ready() {
            var timer = new Timer();
            timer.Interval = 60000 * 15; // update every 15 minutes
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
            
            UpdateActivity();

            return Task.CompletedTask;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e) {
            UpdateActivity();
        }

        private void UpdateActivity() {
            if (isUpdatingUsers)
                return;

            _ = Task.Run(async () => {
                isUpdatingUsers = true;

                var userIds = new List<ulong>();

                foreach (var guild in _client.Guilds) {
                    await guild.GetUsersAsync().ForEachAsync((users) => {
                        foreach (var user in users) {
                            if (userIds.Contains(user.Id)) {
                                continue;
                            }
                            userIds.Add(user.Id);
                        }
                    });
                }

                var status = $"{userIds.Count:N0} AC Players";
                if (lastStatus != status) {
                    await _log.LogAsync(new LogMessage(LogSeverity.Verbose, "BotActivityService", $"Updating activity to: {status}"));
                    await _client.SetActivityAsync(new Game(status, ActivityType.Listening, ActivityProperties.None));
                    lastStatus = status;
                }

                isUpdatingUsers = false;
            });
        }
    }
}
