﻿using ACHelperBot.Lib;
using ACHelperBot.Models;
using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;

namespace ACHelperBot.Services {
    public class DecalUpdateCheckService {
        private DiscordSocketClient _client;
        private ILogService _log;

        private static string PluginsXmlUrl = "https://update.decaldev.com/updatelist/decalplugins.xml?a";

        private static Dictionary<string, PluginXmlEntry> Plugins = new Dictionary<string, PluginXmlEntry>();

        public static Version LatestDecalVersion { get; private set; } = new Version(2, 9, 7, 5);

        public double UpdateIntervalMS { get => 1000 * 60 * 60; } // 1 hour

        public DecalUpdateCheckService(DiscordSocketClient client, ILogService logger) {
            _client = client;
            _log = logger;
            _client.Ready += Client_Ready;

            _log.LogAsync(new LogMessage(LogSeverity.Verbose, "DecalUpdateCheckService", $"Initialized"));
        }

        public Version GetLatestPluginVersion(string classId) {
            if (Plugins.ContainsKey(classId))
                return Plugins[classId].Version;
            return new Version(0, 0, 0);
        }

        public string GetPluginWebsite(string classId) {
            if (Plugins.ContainsKey(classId))
                return Plugins[classId].Website;
            return null;
        }

        private Task Client_Ready() {
            var timer = new Timer();
            timer.Interval = UpdateIntervalMS;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();

            FetchPluginVersions();
            FetchDecalVersion();

            return Task.CompletedTask;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e) {
            FetchDecalVersion();
            FetchPluginVersions();
        }

        private void FetchDecalVersion() {
            _ = Task.Run(async () => {
                // ehhhhhhhhh
                var contents = await LinkDownloader.DownloadFileAsync("https://decaldev.com/");
                var versionRe = new Regex("<meta name=\"version\\-release\" content=\"(?<version>[^\"]+)\" \\/>", RegexOptions.Multiline);
                if (versionRe.IsMatch(contents)) {
                    var matches = versionRe.Match(contents);
                    try {
                        var version = matches.Groups["version"].Value;
                        if (version != null && version.Length > 3) {
                            LatestDecalVersion = new Version(version);
                            Console.WriteLine($"Latest decal version is: {LatestDecalVersion}");
                        }
                    }
                    catch (Exception ex) {
                        Console.WriteLine(ex.ToString());
                    }
                }
                else {
                    Console.WriteLine($"Did not find decal version in source");
                }
            });
        }

        private void FetchPluginVersions() {
            _ = Task.Run(async () => {
                try {
                    var contents = await LinkDownloader.DownloadFileAsync(PluginsXmlUrl);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(contents);

                    var latestVersion = new Version(xmlDoc.DocumentElement.GetAttribute("version"));
                    if (latestVersion > LatestDecalVersion) {
                        LatestDecalVersion = latestVersion;
                    }

                    var plugins = xmlDoc.GetElementsByTagName("plugin");
                    var newPlugins = new Dictionary<string, PluginXmlEntry>();
                    foreach (XmlElement plugin in plugins) {
                        var pluginEntry = new PluginXmlEntry() {
                            ClassId = plugin.GetAttribute("clsid"),
                            Name = plugin.GetAttribute("name"),
                            VersionString = plugin.GetAttribute("version"),
                            Website = plugin.GetAttribute("codebase")
                        };

                        if (string.IsNullOrEmpty(pluginEntry.ClassId)) {
                            await _log.LogAsync(new LogMessage(LogSeverity.Error, "DecalUpdateCheckService", $"Empty ClassId in Plugins.xml: {pluginEntry.Name} // {pluginEntry.VersionString}"));
                            continue;
                        }

                        if (newPlugins.ContainsKey(pluginEntry.ClassId)) {
                            await _log.LogAsync(new LogMessage(LogSeverity.Error, "DecalUpdateCheckService", $"Duplicate ClassId in Plugins.xml: {pluginEntry.ClassId}. One: {newPlugins[pluginEntry.ClassId].Name} Two: {pluginEntry.Name}"));
                            newPlugins.Remove(pluginEntry.ClassId);
                        }

                        newPlugins.Add(pluginEntry.ClassId, pluginEntry);
                    }

                    Plugins = newPlugins;
                }
                catch (Exception ex) {
                    await _log.LogAsync(new LogMessage(LogSeverity.Error, "DecalUpdateCheckService", "Unable to fetch latest plugins.xml", ex));
                }
            });
        }
    }
}
