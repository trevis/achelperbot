﻿using ACHelperBot.Lib;
using ACHelperBot.Models;
using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ACHelperBot.Services {
    // this has been replaced by DecalUpdateCheckService, but keeping it around for now...
    public class GitlabUpdateCheckService {
        private DiscordSocketClient _client;
        private ILogService _log;

        private static Dictionary<string, string> Repos = new Dictionary<string, string>() {
            //{ "UtilityBelt", "https://gitlab.com/api/v4/projects/10819053/releases" }
        };

        private static Dictionary<string, Version> RepoVersions = new Dictionary<string, Version>();

        public GitlabUpdateCheckService(DiscordSocketClient client, ILogService logger) {
            _client = client;
            _log = logger;
            _client.Ready += Client_Ready;

            _log.LogAsync(new LogMessage(LogSeverity.Verbose, "GitlabUpdateCheckService", $"Initialized"));
        }

        public Version GetRepoVersion(string repo) {
            if (RepoVersions.ContainsKey(repo))
                return RepoVersions[repo];
            return new Version(0, 0, 0);
        }

        private Task Client_Ready() {
            var timer = new Timer();
            timer.Interval = 1000 * 60 * 60; // update every hour
            timer.Elapsed += Timer_Elapsed;
            timer.Start();

            UpdateGitlabRepoVersions();

            return Task.CompletedTask;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e) {
            UpdateGitlabRepoVersions();
        }

        private void UpdateGitlabRepoVersions() {
            _ = Task.Run(async () => {
                foreach ((var repoName, var repoUrl) in Repos) {
                    try {
                        var contents = await LinkDownloader.DownloadFileAsync(repoUrl);
                        var tags = JsonConvert.DeserializeObject<GitLabTagData[]>(contents);

                        if (tags == null || tags.Count() == 0)
                            return;

                        var version = new Version(tags[0].tag_name.Replace("release-", ""));

                        if (RepoVersions.ContainsKey(repoName))
                            RepoVersions[repoName] = version;
                        else
                            RepoVersions.Add(repoName, version);
                    }
                    catch (Exception ex) {
                        await _log.LogAsync(new LogMessage(LogSeverity.Error, "GitlabUpdateCheckService", $"Error pulling latest version info for {repoName}", ex));
                    }
                }
            });
        }
    }
}
