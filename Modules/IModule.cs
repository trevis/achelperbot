﻿using ACHelperBot.Models;
using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Modules {
    [DontAutoRegister]
    public abstract class IModule : InteractionModuleBase<SocketInteractionContext> {
        public async Task SendSupportMessageAsync(SupportMessage message) {
            await RespondAsync(message.Text, null, false, message.Ephemeral, null, null, null, message.Embed);
        }
    }
}
