﻿using ACHelperBot.Services;
using ACHelperBot.Models;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.Global {
    /// <summary>
    /// Provides SlashCommand / UserContentCommand / MessageContextCommand for showing instructions to a user on
    /// how to upload a decal export for the bot to check. This is an IGlobalModule so commands are available on
    /// all guilds
    /// </summary>
    public class DecalExportInstructionsCommand : IGlobalModule {
        private readonly ILogService _log;

        public DecalExportInstructionsCommand(ILogService logger) {
            _log = logger;
        }

        /// <summary>
        /// Registers a slash command called "export", that takes an optional user argument.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [SlashCommand("export", "Show directions on how to provide a decal export")]
        public async Task Export_SlashCommand([Summary(description: "The user to ping with export instructions")] IUser user = null) {
            await ShowInstructions(user);
        }

        /// <summary>
        /// Registers a user context menu command.  This is shown when a user is right clicked, under Apps
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [UserCommand("AC Export Instructions")]
        public async Task Export_UserContextCommand(IUser user = null) {
            await ShowInstructions(user);
        }

        /// <summary>
        /// Registers a message context menu command.  This is shown when a message is right clicked, under Apps
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [MessageCommand("AC Export Instructions")]
        public async Task Export_MessageContextCommand(IMessage message) {
            await ShowInstructions(message?.Author);
        }

        /// <summary>
        /// Shows decal export instructions and mentions the supplied user if passed
        /// </summary>
        /// <param name="user">user to mention</param>
        /// <returns></returns>
        private async Task ShowInstructions(IUser user = null) {
            try {
                var forUser = user == null ? "null" : user.Username;
                var fromUser = Context.User == null ? "null" : Context.User.Username;
                var instructions = ACHelperBot.Models.DecalExport.Directions;
                await _log.LogAsync(new LogMessage(LogSeverity.Info, "DecalExportInstructions", $"Got export instructions command for {forUser} from {fromUser}"));
                await RespondAsync(user == null ? instructions : $"{user.Mention} {instructions}");
            }
            catch (Exception ex) {
                await _log.LogAsync(new LogMessage(LogSeverity.Error, "DecalExportInstructions", "Error showing instructions", ex));
            }
        }
    }
}
