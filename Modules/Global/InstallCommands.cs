﻿using ACHelperBot.Services;
using ACHelperBot.Models;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.Global {
    /// <summary>
    /// Provides SlashCommand / UserContentCommand / MessageContextCommand for showing instructions to a user on
    /// how to install
    /// </summary>
    public class InstallCommands : IGlobalModule {
        private readonly ILogService _log;

        private static string FreshInstallInstructions = @"Always choose the default install locations. Failure to adhere will void your warranty.
Follow the instructions to install [Asheron's Call](<https://emulator.ac/how-to-play>) via ACEmulator 
[Thwarglauncher](<http://thwargle.com/>)
[Decal](<https://decaldev.com/>) (read the release notes for up-to-date prerequisites)
- Once installed, decal will ask for the acclient location. Select C:\Turbine\Asheron's Call.
- Decal will always say your acclient is out of date on first run. Click okay then click Update on decal.
- If you still have all red X's ensure you replaced your client and dats per the [Asheron's Call](<https://emulator.ac/how-to-play>) install instructions.
- Post a decal export if you continue to have issues.

Plugins:
[Prerequisites](<https://files.treestats.net/>) (dx9 and vcredist2005)
[Virindi Bundle](<http://virindi.net/plugins/>) (unzip and install)
[UtilityBelt](<https://utilitybelt.gitlab.io/>)

If installing plugins that only provide a .dll, make a ``C:\Games\Decal Plugins\PluginName`` folder and place it there.
Right click the .dll and click properties then choose the Unblock option.
Decal -> Add -> Browse to the folder and select the .dll

For additional plugins visit [ACCPP](<https://www.accpp.net/latest-decal-plugins>)";

        private static string ReinstallInstructions = @"Uninstall Decal via Add/Remove Programs
Delete the C:\Games\VirindiPlugins Folder (backup your VirindiTank folder to retain profiles)
Download [Decal Registry Cleaner](<http://skunkworks.sourceforge.net/misc/CleanDecalReg.exe>)
Download and unzip [VVS Registry Cleaner](<http://www.virindi.net/junk/VVS_Clean.zip>)

Run both cleaners once normal, and once as administrator. Accept any prompts.

Reinstall [Decal](<https://decaldev.com/>)
Reinstall [Virindi Bundle](<http://virindi.net/plugins/>) (unzip and install)
Reinstall or re-add any remaining plugins.";

        public InstallCommands(ILogService logger) {
            _log = logger;
        }

        [SlashCommand("acinstall", "Show directions on how to install a fresh copy of ac + plugins.")]
        public async Task Export_acinstall_SlashCommand([Summary(description: "The user to ping with install instructions")] IUser user = null) {
            await ShowFreshInstallInstructions(user);
        }

        [SlashCommand("acreinstall", "Show directions on how to clean old install and then re-install a fresh copy of ac + plugins.")]
        public async Task Export_acreinstall_SlashCommand([Summary(description: "The user to ping with install instructions")] IUser user = null) {
            await ShowReinstallInstructions(user);
        }

        [UserCommand("AC Fresh Install")]
        public async Task Export_acinstall_UserContextCommand(IUser user = null) {
            await ShowFreshInstallInstructions(user);
        }

        [UserCommand("AC RegClean / Reinstall")]
        public async Task Export_acreinstall_UserContextCommand(IUser user = null) {
            await ShowReinstallInstructions(user);
        }

        [MessageCommand("AC Fresh Install")]
        public async Task Export_acinstall_MessageContextCommand(IMessage message) {
            await ShowFreshInstallInstructions(message?.Author);
        }

        [MessageCommand("AC RegClean / Reinstall")]
        public async Task Export_acreinstall_MessageContextCommand(IMessage message) {
            await ShowReinstallInstructions(message?.Author);
        }

        private async Task ShowFreshInstallInstructions(IUser user = null) {
            try {
                var forUser = user == null ? "null" : user.Username;
                var fromUser = Context.User == null ? "null" : Context.User.Username;
                await _log.LogAsync(new LogMessage(LogSeverity.Info, "InstallCommands", $"Got clean install command for {forUser} from {fromUser}"));
                await RespondAsync(user == null ? FreshInstallInstructions : $"{user.Mention} {FreshInstallInstructions}");
            }
            catch (Exception ex) {
                await _log.LogAsync(new LogMessage(LogSeverity.Error, "InstallCommands", "Error showing instructions", ex));
            }
        }

        private async Task ShowReinstallInstructions(IUser user = null) {
            try {
                var forUser = user == null ? "null" : user.Username;
                var fromUser = Context.User == null ? "null" : Context.User.Username;
                await _log.LogAsync(new LogMessage(LogSeverity.Info, "InstallCommands", $"Got re-install command for {forUser} from {fromUser}"));
                await RespondAsync(user == null ? ReinstallInstructions : $"{user.Mention} {ReinstallInstructions}");
            }
            catch (Exception ex) {
                await _log.LogAsync(new LogMessage(LogSeverity.Error, "InstallCommands", "Error showing instructions", ex));
            }
        }
    }
}
