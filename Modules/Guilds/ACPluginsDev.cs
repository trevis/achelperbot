﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.Guilds {
    /// <summary>
    /// A test guild module that can be used as an example. This is an IGuildModules so commands
    /// registered here are only available for the guild ID specified in GetGuildID()
    /// </summary>
    [DontAutoRegister]
    public class ACPluginsDev : IGuildModule {
        private readonly ILogService _log;
        private readonly DiscordSocketClient _client;

        private const ulong ACE_CLASSIC_USER = 1000889828692467853;
        private const ulong GDLE_CLASSIC_USER = 1000889541978243093;
        private const ulong ACE_USER = 1000889112770912276;
        private const ulong GDLE_USER = 1000889727936905226;
        private const ulong ACE_CUSTOM_USER = 1001315476510621767;
        private const ulong GDLE_CUSTOM_USER = 1001315476510621767;

        /// <summary>
        ///  guild / role id combos
        /// </summary>
        private Dictionary<ulong, ulong> _guildRoleLookup = new Dictionary<ulong, ulong>() {
            // ACE Classic PvP
            { 991822745656643714, ACE_CLASSIC_USER },
            // ACE Classic
            { 976661229055672350, ACE_CLASSIC_USER },
            // PodTide
            { 905184428224024586, ACE_USER },
            // LostWoodsAC
            { 756519838935154778, ACE_CUSTOM_USER },
            // DerpTide
            { 840971306966974494, ACE_CUSTOM_USER },
            // Coldeve
            { 560615844456169512, ACE_USER },
            // LeafDawn
            { 687755192929616186, ACE_CUSTOM_USER },
            // A'Chard
            { 535150588564996101, ACE_USER },
            // asheron4fun
            { 741057296120938496, ACE_CUSTOM_USER },
            // FunkyTown 2.0
            { 726505326156972092, GDLE_CUSTOM_USER },
            // Frostcull
            { 794665536247037972, ACE_CUSTOM_USER },
            // GDLE Official (Reefcull / Harvestbud)
            { 380130170855620609, GDLE_USER },
            // Infinite Frosthaven
            { 961778051073589268, ACE_CUSTOM_USER },
            // Jellocull
            { 678718401581023263, ACE_USER },
            // Morntide
            { 928192918714474516, ACE_USER },
            // Seedsow / SnowReap
            { 405901067394613248, GDLE_CLASSIC_USER },
            // Thistlecrown
            { 714535033821593720, ACE_CUSTOM_USER },
            // ValHeel
            { 972536637772939295, ACE_CUSTOM_USER },

        };

        public ACPluginsDev(ILogService logger, DiscordSocketClient client) {
            _log = logger;
            _client = client;
        }

        /// <summary>
        /// Set this to return the guild ID for this modules guild
        /// </summary>
        /// <returns></returns>
        public override ulong GetGuildID() {
            return 548626271492636675;
        }

        /// <summary>
        /// Watches *all* chat messages and automatically tags a user "server type" roles like ace/gdle/classic
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*")]
        public async Task<SupportMessage> TestPatternChatHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ThingsAndStuff", $"TestPatternChatHandler was triggered in {message?.Channel?.Name}"));

            // you can return null here if you dont want to send a response
            return null;
        }

        /*
        /// <summary>
        /// Watches *all* chat messages and automatically tags a user "server type" roles like ace/gdle/classic
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*")]
        public async Task<SupportMessage> AllChatHandler(IMessage message) {
            if (!message.Author.IsBot) {
                var author = (IGuildUser)message.Author;
                await CheckUserRoles(author);
            }

            // you can return null here if you dont want to send a response
            return null;
        }

        private async Task CheckUserRoles(IGuildUser author) {
            var thisGuild = (IGuild)_client.GetGuild(GetGuildID());
            var roleText = string.Join(", ", author.RoleIds.Select(r => thisGuild.GetRole(r).Name));
            //await _log.LogAsync(new LogMessage(LogSeverity.Info, "ACPluginsDev", $"Checking roles for user {author.Username} with roles {roleText}"));

            foreach (var (guildid, roleid) in _guildRoleLookup) {
                var role = thisGuild.GetRole(roleid);
                if (role == null) {
                    await _log.LogAsync(new LogMessage(LogSeverity.Warning, "ACPluginsDev", $"Could not find role: {roleid}"));
                    continue;
                }
                var guild = _client.GetGuild(guildid);
                if (guild == null) {
                    // we are still waiting on the bot to be added to all the server guilds above...
                    //await _log.LogAsync(new LogMessage(LogSeverity.Warning, "ACPluginsDev", $"Guild was null while checking roles: {guildid}"));
                    continue;
                }
                await guild.DownloadUsersAsync();
                var guildUser = guild.GetUser(author.Id);
                if (guildUser == null && author.RoleIds.Contains(roleid)) {
                    await _log.LogAsync(new LogMessage(LogSeverity.Info, "ACPluginsDev", $"User {author.Username} needs role {role} removed"));
                    await author.RemoveRoleAsync(role);
                }
                else if (guildUser != null && !author.RoleIds.Contains(roleid)) {
                    await _log.LogAsync(new LogMessage(LogSeverity.Info, "ACPluginsDev", $"User {author.Username} needs role {role} added"));
                    await author.AddRoleAsync(role);
                }
                else {
                    //await _log.LogAsync(new LogMessage(LogSeverity.Info, "ACPluginsDev", $"Nothing to do for user: {author.Username} {guildUser == null} {author.RoleIds.Select(t => t.ToString())}"));
                }
            }
        }
        */
    }
}
