﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.Guilds {
    /// <summary>
    /// A test guild module that can be used as an example. This is an IGuildModules so commands
    /// registered here are only available for the guild ID specified in GetGuildID()
    /// </summary>
    [DontAutoRegister]
    public class ThingsAndStuff : IGuildModule {
        private readonly ILogService _log;

        public ThingsAndStuff(ILogService logger) {
            _log = logger;
        }

        /// <summary>
        /// Set this to return the guild ID for this modules guild
        /// </summary>
        /// <returns></returns>
        public override ulong GetGuildID() {
            return 380154028962742272;
        }

        /// <summary>
        /// Registers a message context menu command for this guild.  This is shown when a message is right clicked, under Apps
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [MessageCommand("Things and Stuff A")]
        public async Task ThingsAndStuff1_MessageContextCommand(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ThingsAndStuff", $"[{Context.User.Username}] clicked a message [by {message.Author?.Username} in {message.Channel.Name}] -> Things and Stuff 1"));

            var supportMessage = new SupportMessage($"That decal export looks good to me. Try the tips below if you are still having issues.");
            supportMessage.AddEmbedFieldText("Tips", "Press Alt+Enter at game start to *exit* fullscreen");
            supportMessage.AddEmbedFieldText("Tips", "Make sure your client/dat files are up-to-date");
            supportMessage.AddEmbedFieldText("Tips", "Launch ThwargLauncher as administrator");
            supportMessage.AddEmbedFieldText("Tips", "Make sure the server name in thwarglauncher *exactly* matches the ingame server name");
            supportMessage.AddEmbedFieldText("Tips", "If your game closes during character creation, close thwarglauncher during creation");
            supportMessage.AddEmbedFieldText("Tips", "Close all bloatware in system tray: audio/gfx managers especially");
            supportMessage.AddEmbedFieldText("Tips", "Make sure you follow *all* the install steps for [GDLE](https://www.gdleac.com/#installing) or [ACE](https://emulator.ac/how-to-play/)");

            await SendSupportMessageAsync(supportMessage);
        }

        /// <summary>
        /// Registers a user context menu command for this guild.  This is shown when a user is right clicked, under Apps
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [UserCommand("Things and Stuff B")]
        public async Task ThingsAndStuff2_UserContextCommand(IUser user = null) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ThingsAndStuff", $"[{Context.User.Username}] clicked a user [{user.Username}] -> Things and Stuff 2"));

            var supportMessage = new SupportMessage($"You have issues with that export that may require attention. Follow the steps below:");
            supportMessage.AddEmbedFieldText("Errors", "You have both the original GoArrow and GoArrow (VVS Edition) installed. You should remove one of them. [Details](https://google.com/)");
            supportMessage.AddEmbedFieldText("Errors", "Bad Launcher App set in decal. Repair decal using the installer and choose your `acclient.exe` on the first run. [Details](https://google.com/)");
            supportMessage.AddEmbedFieldText("Errors", "The following decal components are missing DLLs (either moved or deleted). You will need to reinstall them: Plugin:Virindi Chat System 5 [Details](https://google.com/)");
            supportMessage.AddEmbedFieldText("Warnings", "If you are using ThwargLauncher you need to add ThwargFilter.dll to decal. If you are not, you can ignore this message. [Details](https://google.com/)");

            await SendSupportMessageAsync(supportMessage);
        }

        /// <summary>
        /// Watches chat message bodies / image ocr text in this guild for `test.*pattern` and fires the handler when seen
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"test.*pattern")]
        public async Task<SupportMessage> TestPatternChatHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ThingsAndStuff", $"TestPatternChatHandler was triggered in {message?.Channel?.Name}"));

            // you can return null here if you dont want to send a response
            return new SupportMessage($"TestPatternChatHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Mention}");
        }

        /// <summary>
        /// Watches for decal exports and runs when one is found
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        //[DecalExportCommand]
        public async Task<SupportMessage> DecalExportHandler(IMessage message, Models.DecalExport export) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "ThingsAndStuff", $"TestPatternImageHandler was triggered in {message?.Channel?.Name}"));

            // you can return null here if you dont want to send a response
            return new SupportMessage($"DecalExportHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Mention}");
        }
    }
}
