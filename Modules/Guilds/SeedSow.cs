﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.Guilds {
    /// <summary>
    /// Custom responses / commands for SeedSow (GDLE classic server)
    /// </summary>
    [DontAutoRegister]
    public class SeedSow : IGuildModule {
        private readonly ILogService _log;

        public SeedSow(ILogService logger) {
            _log = logger;
        }

        /// <summary>
        /// Set this to return the guild ID for this modules guild
        /// </summary>
        /// <returns></returns>
        public override ulong GetGuildID() {
            return 405901067394613248;
        }

        /// <summary>
        /// help list
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@"help list")]
        public async Task<SupportMessage> HelpListPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"HelpListPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("The following help options are available");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Options", "`bubbles help | bubbles support bot` (Stuck in bubbles animation during login)");
            response.AddEmbedFieldText("Options", "`curse help | curse support bot` (VTank + UB stops after buffing 5 spells)");
            response.AddEmbedFieldText("Options", "`dats help | dats support bot` (Connection to server failure from wrong dats)");
            response.AddEmbedFieldText("Options", "`dungeon maps help | dungeon maps support bot` (Dungeon Maps fix for VTank)");
            response.AddEmbedFieldText("Options", "`fletch help | fletch support bot` (VTank deadly arrows fletching issue)");
            response.AddEmbedFieldText("Options", "`goarrow help | goarrow support bot` (Updating Goarrow, removing TN from Goarrow)");
            response.AddEmbedFieldText("Options", "`PFC help | PFC support bot` (PatchForClassic, infinite buff loop, spelltable)");
            response.AddEmbedFieldText("Options", "`prereqs help | prereqs support bot` (C++ SP1 error/ missing prereqs)");
            response.AddEmbedFieldText("Options", "`server help | server support bot` (Can't find server in Thwarg browser, manual server IP)");
            response.AddEmbedFieldText("Options", "`vtank help | vtank support bot` (VTank combat error, HRESULT)");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// vcredist
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*((C\+\+.*SP1)|prereqs (support bot|help)).*")]
        public async Task<SupportMessage> VCRedistPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"HelpListPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "Make sure you have installed vcredist_x86 from <https://files.treestats.net/>");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// purple bubbles
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*((connect|enter|log|inject|stuck).*(((purple|blue) bubble)|(in portal|in the portal|portaling)))|bubbles (help|support bot).*")]
        public async Task<SupportMessage> PurpleBubblesPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"PurpleBubblesPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "In Decal click Export -> tick the Location checkbox at the top left of the export window -> Copy to Clipboard -> paste here.");
            response.AddEmbedFieldText("Tips", "You may need to reinstall Decal. If you do, ensure after installation that Decal is pointing to EoR dats and NOT Dark Majesty dats.");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// fletching deadlies
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*(((how do I|how can I|can't|won't|wouldn't|not|doesn't|isn't) (fletch|make|craft)).*(deadly|deadlies))|fletch (help|support bot).*")]
        public async Task<SupportMessage> FletchingDeadliesPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"FletchingDeadliesPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "If VTank is not fletching deadly arrows, see [this pin](https://discord.com/channels/405901067394613248/548552076817596432/827418214685605888)");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// PatchForClassic
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*((cast|repeat|defender|Expected spell name).*(the same spell|(infinite|buff|defender|weapon|Aura).*(repeat|loop|over and over|buff|(spelltable|spell table)))|(PFC (support bot|help))).*")]
        public async Task<SupportMessage> PatchForClassicPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"PatchForClassicPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Patch for Classic", "In order to get VTank to buff mastery and items without an infinite loop (or without a VTank error about the spelltable), you MUST have pointed Decal to EoR dats (not Seedsow dats) during installation");
            response.AddEmbedFieldText("Patch for Classic", "Then, you must be using UB's 0.2.4 release or later <http://utilitybelt.gitlab.io/>.");
            response.AddEmbedFieldText("Patch for Classic", "In-game for UB, find Settings -> VTank -> PatchForClassic and set it to true.");
            response.AddEmbedFieldText("Patch for Classic", "You MUST then exit the client entirely, then open the client back up.");
            response.AddEmbedFieldText("Patch for Classic", "Repeat this process for each toon you need the patch to work for.");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// Reijyng's Curse
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*((stop|cast|buff).*(after|five|5).*(life magic|creature spells))|curse (help|support bot).*")]
        public async Task<SupportMessage> ReijyngsCursePatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"ReijyngsCursePatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "If VTank with UB's PatchForClassic will not buff after the first five spells, Click the Buffs tab. Click `Add` under Blacklist. Type `Light Weapon` and click all buffs there.");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// wrong dats
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*((client|fail|connection|((try|trying|tried) to)).*(log|establish|server|connect).*(fail|connection|lost|disconnect))|dats (help|support bot).*")]
        public async Task<SupportMessage> WrongDatsPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"WrongDatsPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Tips", "If the servers are online, make sure your Thwarglauncher is pointing to Seedsow's dats. You can find our dats at <https://seedsow.ca/>.");
            response.AddEmbedFieldText("Tips", "If that fails, in Decal click Export -> tick the Location checkbox at the top left of the export window -> Copy to Clipboard -> paste here.");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// thwarglauncher update / server address
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*(((server address.*not)|(can't.*server)).*(launcher|thwarg|browse list|browser))|server (help|support bot).*")]
        public async Task<SupportMessage> ThwargServerAddressPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"ThwargServerAddressPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("Thwarglauncher Server Address", "If you can't find our server in Thwarglauncher -> Advanced View -> Edit Servers -> Browse Servers, then update your Thwarglauncher or choose Edit Servers -> Add Server.");
            response.AddEmbedFieldText("Thwarglauncher Server Address", "The Server IP for Seedsow is serafino.ddns.net with Server Port 9060.");
            response.AddEmbedFieldText("Thwarglauncher Server Address", "The Server IP for Snowreap is serafino.ddns.net with Server Port 9070.");
            response.AddEmbedFieldText("Thwarglauncher Server Address", "Ensure that the top right GDL Server bubble is marked.");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// VTank combat error
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*(HRESULT|COM component|vtank (help|support bot)).*")]
        public async Task<SupportMessage> VTankCombatErrorPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"VTankCombatErrorPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("VTank Fixes", "In VTank, click Advanced Options -> MeleeCombat then set UseRecklessness and SummonPets to False.");
            response.AddEmbedFieldText("VTank Fixes", "Click the Monsters tab. Make sure only `A` is checked (if a mage, you can use `I/Y/V` if you want).");
            response.AddEmbedFieldText("VTank Fixes", "Click the Buffs tab. Click `Add` under Blacklist. Type `sneak` (click all buffs there); type `shield` (click all buffs there); type `Reck` (click all buffs there); type `Dual` (click all buffs there); type `Two` (click all buffs there).");
            response.AddEmbedFieldText("VTank Fixes", "If that doesn't work, you may need to reinstall Decal. If you do, ensure after installation that Decal is pointing to EoR dats and NOT Seedsow's dats and in Decal click Export -> tick the Location checkbox at the top left of the export window -> Copy to Clipboard -> paste here.");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// GoArrow
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*((go.*arrow|remove).*(TN|Town Network|((won't|doesn't|isn't|to) work)))|goarrow (help|support bot).*")]
        public async Task<SupportMessage> GoArrowPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"GoArrowPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("GoArrow Instructions", "Set your GoArrow -> Atlas -> Update URL to this link: <http://zakarotd.com/Seedsow-GoArrow-Locations.xml>");
            response.AddEmbedFieldText("GoArrow Instructions", "For the updated list to show up in-game you will need to click the update button twice, you will know the update has properly worked when the number of locations in your goarrow changes.");
            response.AddEmbedFieldText("GoArrow Instructions", "If all else fails, use this instead: <http://maps.roogon.com/downloads/data_cod_Non_TN_Non_Olthoi.xml>");

            // you can return null here if you dont want to send a response
            return response;
        }

        /// <summary>
        /// DungeonMaps
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [ChatTextPatternCommand(@".*(((fix|problem.*with).*(dungeon maps))|((dungeon maps).*(broken|not working|isn't working)))|dungeon maps (help|support bot).*")]
        public async Task<SupportMessage> DungeonMapsPatternHandler(IMessage message) {
            await _log.LogAsync(new LogMessage(LogSeverity.Info, "SeedSow", $"DungeonMapsPatternHandler was triggered in {message?.Channel?.Name} by {message?.Author?.Username}"));

            var response = new SupportMessage("");
            response.MessageReference = new MessageReference(message.Id);
            response.AddEmbedFieldText("GoArrow Dungeon Maps", "Place the Dungeon Map Cache folder inside your GoArrow directory and you should be able to view the maps in game: <https://drive.google.com/uc?export=download&id=1PzJMuxqIumpSNba-xTlBBQw-h8kbpteG>.");
            response.AddEmbedFieldText("GoArrow Dungeon Maps", "If you have issues with the maps no longer working after a while, try this version of goarrow.dll: <https://github.com/kaldorgreybear/AsheronsCall-VTGoArrow>.");
            response.AddEmbedFieldText("GoArrow Dungeon Maps", "There is a link to download the compiled dll in the readme section of the github page.");

            // you can return null here if you dont want to send a response
            return response;
        }
    }
}
