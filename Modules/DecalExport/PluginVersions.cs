﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.DecalExport {
    public class PluginVersions : IDecalExportModule {
        private ILogService _log;
        private DecalUpdateCheckService _decalUpdates;

        public PluginVersions(ILogService logger, DecalUpdateCheckService decalUpdates) {
            _log = logger;
            _decalUpdates = decalUpdates;
        }

        /// <summary>
        /// Check that plugins are up to date
        /// </summary>
        /// <returns></returns>
        [DecalExportRule]
        private async Task<SupportMessage> CheckPluginVersions(IMessage message, Models.DecalExport decalExport) {
            var response = new SupportMessage("");
            foreach (var component in decalExport.Components) {
                var latestVersion = _decalUpdates.GetLatestPluginVersion(component.ClassId);
                var website = _decalUpdates.GetPluginWebsite(component.ClassId);
                if (!string.IsNullOrEmpty(website) && latestVersion != null && !component.NoVersion && component.Version < latestVersion) {
                    response.AddEmbedFieldText("Out of Date Plugins", $"{component.Name} is out of date. ({component.Version} vs {latestVersion}). Update it [here]({website}).");
                }
            }

            return response.EmbedFields.Count > 0 ? response : null;
        }
    }
}
