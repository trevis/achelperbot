﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.DecalExport {
    public class LifeTankX : IDecalExportModule {

        private ILogService _log;

        public LifeTankX(ILogService logger) {
            _log = logger;
        }

        /// <summary>
        /// Check if lifetank x is installed, and recommend vtank.
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckIfExists(IMessage message, Models.DecalExport decalExport) {
            var lifeTankX = decalExport.Plugins.Where(f => f.Name == "LifeTank X").FirstOrDefault();

            if (lifeTankX != null && lifeTankX.Enabled > 0) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Warnings", $"LifeTank X is a defunct plugin. You should replace it with vTank: http://www.virindi.net/plugins/");
                return response;
            }

            return null;
        }
    }
}
