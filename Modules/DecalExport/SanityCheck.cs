﻿using ACHelperBot.Lib.Attributes;
using ACHelperBot.Models;
using ACHelperBot.Services;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ACHelperBot.Modules.DecalExport {
    public class SanityCheck : IDecalExportModule {
        static RegexOptions ReOpt = RegexOptions.Compiled | RegexOptions.IgnoreCase;

        public static Dictionary<string, Regex> RequiredComponents = new Dictionary<string, Regex>() {
            { "Echo Filter 2", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\DecalFilters\.dll", ReOpt) },
            { "Character Stats Filter", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\DecalFilters\.dll", ReOpt) },
            { "World Object Filter", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\DecalFilters\.dll", ReOpt) },
            { "Decal FileService", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\Decal\.FileService\.dll", ReOpt) },
            { "Identify Queue Filter", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\DecalFilters\.dll", ReOpt) },
            { "Decal Dat Service", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decaldat\.dll", ReOpt) },
            { "Decal .NET Lifetime Service", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\Decal\.Adapter\.dll", ReOpt) },
            { "Decal Input Service", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalInput\.dll", ReOpt) },
            { "Decal Networking Service", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalnet\.dll", ReOpt) },
            { "Decal D3DService", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\D3DService\.dll", ReOpt) },
            { "Decal Render Service", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalrender\.dll", ReOpt) },
            { "Decal Inject Gateway Service",new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\Inject\.dll", ReOpt) },
            { "Version 1 Plugin Surrogate", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\Inject\.dll", ReOpt) },
            { "Prefilter Network Filter Surrogate", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\DecalFilters\.dll", ReOpt) },
            { "Decal.Adapter Surrogate", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\Decal\.Adapter\.dll", ReOpt) },
            { "ActiveX Plugin Surrogate", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\Decal\.dll", ReOpt) },
            { "Delay Input Action", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalInput\.dll", ReOpt) },
            { "Mouse Move Input Action", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalInput\.dll", ReOpt) },
            { "Restore Input Action", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalInput\.dll", ReOpt) },
            { "Polled Delay Input Action", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalInput\.dll", ReOpt) },
            { "Typing Input Action", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalInput\.dll", ReOpt) },
            { "Event Input Action", new Regex(@"[a-z]:\\Program Files( \(x86\))?\\Decal 3\.0\\decalInput\.dll", ReOpt) },
        };

        private ILogService _log;
        private readonly DecalUpdateCheckService _decalUpdates;

        public SanityCheck(ILogService logger, DecalUpdateCheckService decalUpdates) {
            _log = logger;
            _decalUpdates = decalUpdates;
        }

        /// <summary>
        /// Make sure [d3dx9_30.dll] is installed
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckDX9_30(IMessage message, Models.DecalExport decalExport) {
            if (!decalExport.D3DXLibraries.IsInstalled("d3dx9_30.dll")) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Errors", "d3dx9_30.dll is not installed.  You should install dxwebsetup from <https://files.treestats.net/>");
                return response;
            }

            return null;
        }

        /// <summary>
        /// Check decal injection method is set to default
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckSettings(IMessage message, Models.DecalExport decalExport) {
            bool foundIssues = false;

            var response = new SupportMessage("");
            if (decalExport.DecalSettings.InjectionMethod != DecalSettings.InjectionMethodType.Timer) {
                response.AddEmbedFieldText("Errors", $"Open up Decal options and uncheck `Use Alternate Injection Method`");
                foundIssues = true;
            }

            if (!decalExport.DecalSettings.NoMoviesEnabled || !decalExport.DecalSettings.NoSplashEnabled) {
                response.AddEmbedFieldText("Warnings", $"If you are using thwarglauncher you should open up decal options and check both `No Movies` and `No Logos` under Client Patches");
                foundIssues = true;
            }

            if (!decalExport.DecalSettings.DualLogEnabled) {
                response.AddEmbedFieldText("Warnings", $"If you are attempting to dual log, you should open up decal options and check `Dual Log` under Client Patches");
                foundIssues = true;
            }

            return foundIssues ? response : null;
        }

        /// <summary>
        /// Check that the LauncherApp is set to acclient.exe
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckLauncherApp(IMessage message, Models.DecalExport decalExport) {
            if (decalExport.DecalSettings.LauncherApp != "acclient.exe") {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Errors", $"Bad `Launcher App` set in decal. Expected=acclient.exe Actual={decalExport.DecalSettings.LauncherApp} . Run the decal installer, choose repair. Run decal and select the acclient in your ac install directory when prompted");
                return response;
            }

            return null;
        }

        /// <summary>
        /// Check for any missing decal components
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckMissingComponents(IMessage message, Models.DecalExport decalExport) {
            var missingComponents = new List<string>();
            foreach (var kv in RequiredComponents) {
                var component = decalExport.Components.Where(c => c.Name == kv.Key).FirstOrDefault();
                if (component == null) {
                    missingComponents.Add(kv.Key);
                }
            }

            if (missingComponents.Count > 0) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Errors", $"Decal is missing the following required components: {string.Join(", ", missingComponents)}. You may need to reinstall decal.");
                return response;
            }

            return null;
        }

        /// <summary>
        /// Check that plugin / filter dlls exist
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckDllsExist(IMessage message, Models.DecalExport decalExport) {
            List<string> missingDlls = new List<string>();
            List<string> missingDllPaths = new List<string>();
            var components = decalExport.Plugins.Concat(decalExport.NetworkFilters).Concat(decalExport.Services);

            foreach (var component in components) {
                if (component.NoDll) {
                    missingDlls.Add($"\t- {component.ComponentType}: {component.Name}");
                }
                else if (component.ErrorLoadingDLLPath) {
                    missingDllPaths.Add($"\t- {component.ComponentType}: {component.Name}");
                }
            }

            var invalidDlls = missingDlls.Concat(missingDllPaths);

            if (invalidDlls.Count() > 0) {
                var response = new SupportMessage("");
                response.AddEmbedFieldText("Errors", $"The following decal components are missing DLLs (either moved, deleted, or permission issues). You will need to reinstall them:\n {string.Join("\n", invalidDlls)}");
                return response;
            }
            return null;
        }

        /// <summary>
        /// Check that plugins are not installed to weird folders
        /// </summary>
        [DecalExportRule]
        private async Task<SupportMessage> CheckLocations(IMessage message, Models.DecalExport decalExport) {
            List<string> downloadDlls = new List<string>();
            List<string> acDlls = new List<string>();
            List<string> decalDlls = new List<string>();
            var components = decalExport.Plugins.Concat(decalExport.NetworkFilters);
            var response = new SupportMessage("");

            foreach (var component in components) {
                if (string.IsNullOrEmpty(component.Location))
                    continue;

                if (component.Location.ToLower().Contains("downloads")) {
                    downloadDlls.Add($"{component.ComponentType}:{component.Name}");
                }

                if (component.Location.ToLower().Contains(decalExport.DecalSettings.PortalPath.ToLower())) {
                    acDlls.Add($"{component.ComponentType}:{component.Name}");
                }

                if (component.ComponentType == DecalComponentType.Plugin && component.Location.ToLower().StartsWith(@"c:\program files (x86)\decal 3.0\") && component.Name != "Decal Hotkey System") {
                    decalDlls.Add($"{component.Name}");
                }
            }

            if (downloadDlls.Count > 0) {
                response.AddEmbedFieldText("Warnings", $"The following decal components appear to be in your downloads directory, you should move them somewhere more permanent like `C:\\Games\\Decal Plugins\\<Plugin>\\`:\n{string.Join(", ", downloadDlls)}");
            }

            if (acDlls.Count > 0) {
                response.AddEmbedFieldText("Warnings", $"The following decal components appear to be in your ac install directory, you should move them somewhere with their own directory, like `C:\\Games\\Decal Plugins\\<Plugin Name>\\<Plugin>.dll`:\n{string.Join(", ", acDlls)}");
            }

            if (decalDlls.Count > 0) {
                response.AddEmbedFieldText("Warnings", $"The following decal plugins appear to be installed to your decal directory, you should move them somewhere with their own directory, like `C:\\Games\\Decal Plugins\\<Plugin Name>\\<Plugin>.dll`:\n{string.Join(", ", decalDlls)}");
            }

            return response.EmbedFields.Count > 0 ? response : null;
        }
    }
}
