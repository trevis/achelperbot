﻿using System;
using System.Text.RegularExpressions;

namespace ACHelperBot.Lib.Attributes {
    internal class ChatTextPatternCommandAttribute : Attribute {
        public Regex MatchPattern { get; }

        public ChatTextPatternCommandAttribute(string matchPattern) {
            MatchPattern = new Regex(matchPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        }
    }
}