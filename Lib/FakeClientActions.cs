﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Interop;

namespace ACHelperBot.Lib {
    public class FakeClientActions : IClientActionsRaw {
        public FakeClientActions(ILogger log) {

        }
        public FakeClientActions() {

        }

        public uint SelectedObject() {
            throw new NotImplementedException();
        }

        public void SkillAdvance(SkillId skill, uint creditsToSpend) {
            throw new NotImplementedException();
        }

        public void AllegianceBreak(uint objectId) {
            throw new NotImplementedException();
        }

        public void AllegianceSwear(uint objectId) {
            throw new NotImplementedException();
        }

        public void Login(uint id) {
            throw new NotImplementedException();
        }

        public void AttributeAddExperience(AttributeId attribute, uint experienceToSpend) {
            throw new NotImplementedException();
        }

        public void SkillAddExperience(SkillId skill, uint experienceToSpend) {
            throw new NotImplementedException();
        }

        public void VitalAddExperience(VitalId vital, uint experienceToSpend) {
            throw new NotImplementedException();
        }

        public void ApplyWeenie(uint sourceWeenieId, uint targetWeenieId) {
            throw new NotImplementedException();
        }

        public void AutoWield(uint weenieId, EquipMask slot) {
            throw new NotImplementedException();
        }

        public void CastSpell(uint spellId, uint targetId) {
            throw new NotImplementedException();
        }

        public void SetCombatMode(CombatMode combatMode) {
            throw new NotImplementedException();
        }

        public void DropObject(uint weenieId) {
            throw new NotImplementedException();
        }

        public void SplitObject(uint objectId, uint targetId, uint newStackSize, uint slot = 0) {
            throw new NotImplementedException();
        }

        public void FellowshipCreate(string name, bool shareExperience) {
            throw new NotImplementedException();
        }

        public void FellowshipDisband() {
            throw new NotImplementedException();
        }

        public void FellowshipDismiss(uint objectId) {
            throw new NotImplementedException();
        }

        public void FellowshipSetLeader(uint objectId) {
            throw new NotImplementedException();
        }

        public void FellowshipQuit(bool disband) {
            throw new NotImplementedException();
        }

        public void FellowshipRecruit(uint objectId) {
            throw new NotImplementedException();
        }

        public void FellowshipSetOpen(bool open) {
            throw new NotImplementedException();
        }

        public void GiveWeenie(uint weenieId, uint targetWeenieId) {
            throw new NotImplementedException();
        }

        public void InvokeChatParser(string text) {
            throw new NotImplementedException();
        }

        public void Logout() {
            throw new NotImplementedException();
        }

        public void MoveWeenie(uint weenieId, uint containerWeenieId, uint slot, bool stack) {
            throw new NotImplementedException();
        }

        public void Appraise(uint weenieId) {
            throw new NotImplementedException();
        }

        public void SalvagePanelAdd(uint objectId) {
            throw new NotImplementedException();
        }

        public void SalvagePanelSalvage() {
            throw new NotImplementedException();
        }

        public void SelectWeenie(uint weenieId) {
            throw new NotImplementedException();
        }

        public void UseWeenie(uint weenieId) {
            throw new NotImplementedException();
        }

        public void SetAutorun(bool enabled) {
            throw new NotImplementedException();
        }

        public void TradeAccept() {
            throw new NotImplementedException();
        }

        public void TradeAdd(uint objectId) {
            throw new NotImplementedException();
        }

        public void TradeDecline() {
            throw new NotImplementedException();
        }

        public void TradeEnd() {
            throw new NotImplementedException();
        }

        public void TradeReset() {
            throw new NotImplementedException();
        }

        public void VendorAddToBuyList(uint objectId, uint amount = 1) {
            throw new NotImplementedException();
        }

        public void VendorAddToSellList(uint objectId) {
            throw new NotImplementedException();
        }

        public void VendorBuyAll() {
            throw new NotImplementedException();
        }

        public void VendorSellAll() {
            throw new NotImplementedException();
        }

        public void VendorClearBuyList() {
            throw new NotImplementedException();
        }

        public void VendorClearSellList() {
            throw new NotImplementedException();
        }

        public void SendTellByObjectId(uint objectId, string message) {
            throw new NotImplementedException();
        }

        public void AcceptNextConfirmationRequest() {
            throw new NotImplementedException();
        }

        public void CastEquippedWandSpell(uint targetId = 0) {
            throw new NotImplementedException();
        }

        public void Dispose() {
            throw new NotImplementedException();
        }

        public void InscribeWeenie(uint objectId, string inscription) {
            throw new NotImplementedException();
        }
    }
}
