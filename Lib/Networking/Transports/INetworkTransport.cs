﻿using ACClientLib.Lib.Networking.Packets;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ACClientLib.Lib.Networking.Transports {
    public abstract class INetworkTransport : IDisposable {

        public abstract event EventHandler<TransportDataEventArgs> OnData;

        public abstract void Start(string host, int port);
        public abstract void Update();
        public abstract void Send(ServerInfo si, byte[] data, int length, bool useRead);

        public virtual void Dispose() {
            
        }
    }
}
