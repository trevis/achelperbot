using System;
using System.IO;

namespace ACClientLib.Lib.Networking.Packets {
    public class PingEventFragment : EventFragment {
        public PingEventFragment()
            : base(0x01e9) {
        }
    }
}
