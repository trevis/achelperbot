﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ACClientLib.Lib.Networking.Packets {
    class SendEnterWorldFragment : Fragment {
        public int Id;
        public string AccountName;

        public SendEnterWorldFragment(int id, string accountName) : base(0xf657, 4) {
            Id = id;
            AccountName = accountName;
        }

        protected override void OnSerialize(BinaryWriter writer) {
            base.OnSerialize(writer);
            writer.Write(Id);
            writer.WriteString16(AccountName);
        }
    }
}
