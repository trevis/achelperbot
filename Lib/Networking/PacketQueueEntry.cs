﻿using ACClientLib.Lib.Networking.Packets;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Lib.Networking {
    public struct PacketQueueEntry {
        public Packet Packet;
        public ServerInfo Server;
        public bool IncludeSequence;
        public bool IncrementSequence;
        public PacketFlags Flags;

        public PacketQueueEntry(ServerInfo si, Packet p, bool include, bool increment, PacketFlags flags) {
            Server = si;
            Packet = p;
            IncludeSequence = include;
            IncrementSequence = increment;
            Flags = flags;
        }
    }
}
