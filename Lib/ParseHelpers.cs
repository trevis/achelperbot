﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHelperBot.Lib {
    static class ParseHelpers {
        public static (string key, string value) ParseKV(string line) {
            var parts = line.Split(":");

            if (parts.Length < 2)
                return (null, null);

            var key = parts[0].Replace("[", "").Replace("]", "").Trim();
            var value = string.Join(":", parts.Skip(1)).Trim();

            return (key, value);
        }
    }
}
